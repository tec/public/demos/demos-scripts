#!/usr/bin/env python3.8
# -*- coding: UTF-8 -*-
"""
Copyright (c) 2023, ETH Zurich, Computer Engineering Group (TEC)
"""

# Analysis script for the Demos project
#
# Author: abiri
# Date:   19.06.23

import logging
import sys
import os
import flocklab
import pandas                as pd
import datetime              as dt
import numpy                 as np
import xml.etree.ElementTree as ET
from base64   import b64encode
from enum     import IntEnum
from ast      import literal_eval
from argparse import ArgumentParser
from os.path  import isfile, isdir
from time     import sleep
from random   import randint
from shutil   import copyfile

# ----------------------------------------------------------------------------------------------------------------------
# General consts and variables
# ----------------------------------------------------------------------------------------------------------------------

# Default params for command line arguments that are mandatory
FILE_DIR            = os.path.dirname(__file__)
DEFAULT_CONFIG_FILE = '../demos.conf'
DEFAULT_LOG_LEVEL   = 'INFO'

LOG_FORMAT      = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
LOG_DATE_FORMAT = "%H:%M:%S"

FL_XML_TEMPLATE  = '/flocklab_template.xml'
FL_XML_TEST      = '/flocklab_dpp2lora_demos.xml'
FL_TEST_FILE     = '/comboard_demos.elf'
FL_TEST_PATCHED  = '/comboard_demos_patched.elf'
FL_TEST_LOG      = '/flocklab_test.log'
FL_TEST_DIR      = '/tests/'
FL_ACTUATION     = '/flocklab_actuation.log'
FL_GENERATION    = '/generate_actuations.py'
FL_GPIO          = '/gpiotracing.csv'
FL_SERIAL        = '/serial.csv'
FL_MOBILE_SERIAL = '/mobile_serial.txt'
FL_MOBILE_GPIO   = '/mobile_gpio.csv'
FL_STATS_NODES   = '/stats_nodes'
FL_STATS_NETWORK = '/stats_network'
FL_STATS_ACCUR   = '/stats_accuracy'
FL_TIME_FORMAT   = "%Y-%m-%d %H:%M:%S"
FL_NAMESPACE     = 'http://www.flocklab.ethz.ch'
FL_BAUDRATE      = 460800
FL_TRACING_PINS  = ['INT1', 'INT2', 'LED1', 'LED2', 'LED3']
FL_ACTUATION_PIN = 'SIG1'
FL_RESET_PIN     = 'nRST'
FL_NODE_IDS      = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 16, 19, 20, 21, 22, 23, 24, 26, 27, 28, 29, 31, 32]
FL_GNSS_SYNCED   = [1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 15, 17, 20, 24, 25, 29, 30, 31, 32]
FL_REMOTE_IDS    = [15, 17, 25, 30]
FL_EXCLUDED_IDS  = [5, 10]
FL_MOBILE_IDS    = [12, 23]

FL_NETWORK_SMALL  = [1, 2, 4, 8, 11, 12, 16, 20, 24, 27, 29, 32]  # 12 nodes
FL_NETWORK_MEDIUM = FL_NETWORK_SMALL  + [ 3,  6,  7,  9, 26, 31]  # 18 nodes
FL_NETWORK_LARGE  = FL_NETWORK_MEDIUM + [13, 19, 21, 22, 23, 28]  # 24 nodes
FL_NETWORK_SPLIT  = [1, 2, 8, 9, 11, 21, 23, 27, 28, 31, 32]

S_TO_MS = 1000
S_TO_US = 1000 * 1000

RADIO_TX_THRES_CAPTURE = 125 / S_TO_US
RADIO_TX_THRES_CONSTR  =  10 / S_TO_US
RADIO_TX_THRES_SEMI    =  25 / S_TO_US

# ----------------------------------------------------------------------------------------------------------------------
# Classes and Functions
# ----------------------------------------------------------------------------------------------------------------------


class DemosAnalyser:

    def __init__(self):

        self._logger         = logging.getLogger(self.__class__.__name__)
        self.LOG_LVL         = DEFAULT_LOG_LEVEL
        self.LOG_FORMAT      = LOG_FORMAT
        self.LOG_DATE_FORMAT = LOG_DATE_FORMAT

        # Initialize logging - afterwards, can use "self._logger.*" instead of root logger ("logging.*")
        self._init_logging(self.LOG_LVL, self.LOG_FORMAT, self.LOG_DATE_FORMAT)

    def _init_logging(self, log_lvl='DEBUG', format=None, date_format=None):
        self._logger.setLevel(logging.DEBUG)
        self._logger.propagate = False  # Avoid propagation to root logger

        # Setup console output
        ch = logging.StreamHandler()
        ch.setLevel(log_lvl)

        # Create formatter and add it to the handlers
        if format is not None:
            if date_format is None:
                formatter = logging.Formatter(format)
            else:
                formatter = logging.Formatter(fmt=format, datefmt=date_format)

            ch.setFormatter(formatter)

        # Add handlers to logger
        self._logger.addHandler(ch)

    def load_gpio_actuations(self, pin=FL_ACTUATION_PIN, path=FILE_DIR):

        file_path = path + FL_ACTUATION
        if not os.path.isfile(file_path):
            raise TypeError('GPIO actuation file (%s) not found' % (file_path,))

        # Get actuations
        actuation_file = open(file_path, 'r')
        data           = actuation_file.read()
        actuation_file.close()

        # Parse lines
        lines     = data.split('\n')
        rows_list = []

        for line in lines:

            # Make sure line is not empty or not commented out
            if len(line) == 0 or line[0] == '#' or line[:2] == '//':
                continue

            # Extract actuations
            node_id, time_offset_s, duration_s = line.split(',', maxsplit=3)

            row = {'node_id':    int(node_id),
                   'offset_s':   float(time_offset_s),
                   'duration_s': float(duration_s),
                   'pin':        pin}
            rows_list.append(row)

        self._logger.debug('Read %i actuations from %s' % (len(rows_list), file_path,))

        return rows_list

    def generate_xml(self, gpio_actuations=None, path=None, comment_str=None, test_time_s=300, nr_nodes=None, enable_power_profiling=False, apply_patch=None):

        # Verify that FlockLab IDs are up-to-date and available
        fl            = flocklab.Flocklab()
        available_ids = set(fl.getObsIds('DPP2LoRa'))
        test_ids      = set(FL_NODE_IDS) - set(FL_REMOTE_IDS) - set(FL_EXCLUDED_IDS)

        if not test_ids.issubset(available_ids):
            self._logger.warning("Not all given FlockLab IDs are available; expected %s, but available IDs are %s" % (str(sorted(test_ids)), str(sorted(available_ids)),))
            unavailable_ids = []
            for fl_id in test_ids:
                if fl_id not in available_ids:
                    unavailable_ids.append(fl_id)

            for fl_id in unavailable_ids:
                test_ids.remove(fl_id)  # Requires separate loop, as otherwise iterator in first loop jumps the next following ID
            self._logger.warning("Removed IDs %s from the set of nodes to run the test" % (str(unavailable_ids),))

            # Allow user to stop test submission
            countdown_s = 5
            while countdown_s:
                timer_str = 'Submitting test in %us... [press Ctrl+C to abort]' % (countdown_s,)
                print(timer_str, end="\r")
                sleep(1)
                countdown_s -= 1

        if nr_nodes is not None:
            if nr_nodes > len(test_ids):
                self._logger.warning("Insufficient nodes available; requested %i but only %i are available" % (nr_nodes, len(test_ids),))
            elif nr_nodes == len(FL_NETWORK_SMALL):
                if set(FL_NETWORK_SMALL).issubset(test_ids):
                    self._logger.info("Testing small network (%i nodes)" % (nr_nodes,))
                    test_ids = set(FL_NETWORK_SMALL)
                else:
                    self._logger.warning("Could not schedule small network testing, revert to full network")
            elif nr_nodes == len(FL_NETWORK_MEDIUM):
                if set(FL_NETWORK_MEDIUM).issubset(test_ids):
                    self._logger.info("Testing medium network (%i nodes)" % (nr_nodes,))
                    test_ids = set(FL_NETWORK_MEDIUM)
                else:
                    self._logger.warning("Could not schedule medium network testing, revert to full network")
            elif nr_nodes == len(FL_NETWORK_LARGE):
                if set(FL_NETWORK_LARGE).issubset(test_ids):
                    self._logger.info("Testing large network (%i nodes)" % (nr_nodes,))
                    test_ids = set(FL_NETWORK_LARGE)
                else:
                    self._logger.warning("Could not schedule large network testing, revert to full network")
            elif nr_nodes == len(FL_NETWORK_SPLIT):
                if set(FL_NETWORK_SPLIT).issubset(test_ids):
                    self._logger.info("Testing network for splitting (%i nodes)" % (nr_nodes,))
                    test_ids = sorted(FL_NETWORK_SPLIT)
                else:
                    self._logger.warning("Could not schedule network splitting testing, revert to full network")
            else:
                self._logger.warning("Unknown size request, revert to full network")

        # Apply description and custom information
        descr       = comment_str
        custom_info = None

        # Apply binary patching
        if apply_patch is not None:
            if path is None:
                path = FILE_DIR
            bin_path         = path + FL_TEST_FILE
            bin_path_patched = path + FL_TEST_PATCHED

            # Convert string to dictionary
            patch_dict = literal_eval(apply_patch)

            # Apply patching
            copyfile(bin_path, bin_path_patched)
            for k, v in patch_dict.items():
                # self._logger.info('\'{}\': {}'.format(k, v))
                fl.writeSymbolValue(elfPath=bin_path_patched, symbName=k, symbReplace=v, signed=(v < 0))

            # Store info for debugging
            custom_info = patch_dict
            self._logger.info('Patched ELF file using dictionary: %s' % (str(patch_dict),))

        # Create XML
        if path is not None:
            self.create_FL_xml(node_ids=test_ids, gpio_actuations=gpio_actuations, descr=descr, test_time_s=test_time_s, powerprofiling_enabled=enable_power_profiling, custom_info=custom_info, patched=apply_patch is not None, path=path)
        else:
            self.create_FL_xml(node_ids=test_ids, gpio_actuations=gpio_actuations, descr=descr, test_time_s=test_time_s, powerprofiling_enabled=enable_power_profiling, custom_info=custom_info, patched=apply_patch is not None)

    @staticmethod
    def xml_indent(elem, level=0):
        i = "\n" + level * "\t"
        if len(elem):
            if not elem.text or not elem.text.strip():
                elem.text = i + "\t"
            if not elem.tail or not elem.tail.strip():
                elem.tail = "\n" + i if (level == 1) else i
            for elem in elem:
                DemosAnalyser.xml_indent(elem, level + 1)
            if not elem.tail or not elem.tail.strip():
                elem.tail = i
        else:
            if level and (not elem.tail or not elem.tail.strip()):
                elem.tail = i

    def create_FL_xml(self, node_ids, gpio_actuations=None, path=FILE_DIR, pretty_print=True, powerprofiling_enabled=False, test_time_s=300, descr=None, custom_info=None, add_logical_IDs=True, patched=False):

        # Register namespace
        ET.register_namespace('', FL_NAMESPACE)

        # Import template
        tree = ET.parse(FILE_DIR + FL_XML_TEMPLATE)
        root = tree.getroot()

        # Gather necessary information for XML
        if gpio_actuations is None or len(gpio_actuations) == 0:
            self._logger.info('Did not receive any GPIO actuations to schedule')
        elif node_ids is None or len(node_ids) == 0:
            raise ValueError('Did not receive any Flocklab nodes')

        # Import binary data
        if   'demos' in path:
            if patched:
                binary_src_path = path + FL_TEST_PATCHED
                self._logger.info('Using patched ELF file \'%s\'' % (binary_src_path,))
            else:
                binary_src_path = path + FL_TEST_FILE
        elif 'hydra' in path:
            binary_src_path = path + '/comboard_hydra.elf'
        elif 'lwb' in path:
            binary_src_path = path + '/comboard_lwb.elf'
        else:
            raise ValueError('Unknown protocol at path %s' % (path,))

        if not os.path.isfile(binary_src_path):
            raise ValueError('Source file (%s) not found' % (binary_src_path,))
        else:
            data = open(binary_src_path, 'rb').read()
            binary_src = b64encode(data).decode("utf-8")

        # Handle GPIO actuations
        if gpio_actuations is not None:
            # Replace negative durations with duration until the end of the test
            for actuation in gpio_actuations:
                if actuation['duration_s'] < 0:
                    actuation['duration_s'] = test_time_s - actuation['offset_s']
                elif (actuation['offset_s'] + actuation['duration_s']) > test_time_s:
                    actuation['duration_s'] = test_time_s - actuation['offset_s']

            # Embed actuations as custom information if not otherwise defined
            if custom_info is None:
                custom_info = str(gpio_actuations)

            # Convert to Dataframe for filtering
            gpio_actuations = pd.DataFrame(gpio_actuations)

        # Edit XML elements
        target_elem = None
        power_elem  = None

        for elem in root:
            if elem.tag == '{%s}%s' % (FL_NAMESPACE, 'generalConf',):
                for subelem in elem:
                    if subelem.tag == '{%s}%s' % (FL_NAMESPACE, 'schedule',):
                        subelem[0].text = str(test_time_s)
                    elif subelem.tag == '{%s}%s' % (FL_NAMESPACE, 'name',):
                        if 'demos' not in binary_src_path:
                            subelem.text = 'Comparison'
                    elif subelem.tag == '{%s}%s' % (FL_NAMESPACE, 'description',) and descr is not None:
                        subelem.text = str(descr)
                    elif subelem.tag == '{%s}%s' % (FL_NAMESPACE, 'custom',) and custom_info is not None:
                        subelem.text = str(custom_info)
            elif elem.tag == '{%s}%s' % (FL_NAMESPACE, 'targetConf',):
                target_elem = elem
                for subelem in elem:
                    if subelem.tag == '{%s}%s' % (FL_NAMESPACE, 'obsIds',):
                        if max(e.text == 'Image_src' for e in elem):
                            subelem.text = ' '.join(map(str, node_ids))
                        else:
                            self._logger.error('Found invalid target configuration with unknown embeddedImageId')
            elif elem.tag == '{%s}%s' % (FL_NAMESPACE, 'serialConf',):
                for subelem in elem:
                    if subelem.tag == '{%s}%s' % (FL_NAMESPACE, 'obsIds',):
                        subelem.text = ' '.join(map(str, node_ids))
                    elif subelem.tag == '{%s}%s' % (FL_NAMESPACE, 'baudrate',):
                        subelem.text = str(FL_BAUDRATE)
            elif elem.tag == '{%s}%s' % (FL_NAMESPACE, 'gpioTracingConf',):
                for subelem in elem:
                    if subelem.tag == '{%s}%s' % (FL_NAMESPACE, 'obsIds',):
                        subelem.text = ' '.join(map(str, node_ids))
            elif elem.tag == '{%s}%s' % (FL_NAMESPACE, 'gpioActuationConf',):
                self._logger.warning('Found existing \'gpioActuationConf\'')
            elif elem.tag == '{%s}%s' % (FL_NAMESPACE, 'powerProfilingConf',):
                power_elem = elem
                for subelem in elem:
                    if subelem.tag == '{%s}%s' % (FL_NAMESPACE, 'obsIds',):
                        subelem.text = ' '.join(map(str, node_ids))
                    elif subelem.tag == '{%s}%s' % (FL_NAMESPACE, 'duration',):
                        subelem.text = str(test_time_s  - 60)
            elif elem.tag == '{%s}%s' % (FL_NAMESPACE, 'embeddedImageConf',):
                for subelem in elem:
                    if subelem.tag == '{%s}%s' % (FL_NAMESPACE, 'data',):
                        if max(e.text == 'Image_src' for e in elem):
                            subelem.text = str(binary_src)
                        else:
                            self._logger.error('Found invalid embedded image configuration with embeddedImageId')
            else:
                self._logger.warning('Found unknown XML element %s in \'%s\'' % (elem.tag, FILE_DIR + FL_XML_TEMPLATE,))

        # Add target IDs if node IDs should be different from observer IDs
        if add_logical_IDs:
            target_ids_elem      = ET.Element('targetIds')
            target_ids_elem.text = ' '.join([str(node_id) for node_id in list(range(1, len(node_ids) + 1))])
            target_elem.insert(1, target_ids_elem)

            observer_ids = ' '.join(["%2u" % (node_id,) for node_id in node_ids                         ])
            target_ids   = ' '.join(["%2u" % (node_id,) for node_id in list(range(1, len(node_ids) + 1))])
            self._logger.info("Mapping observer IDs to target IDs:\n%s\n%s" % (observer_ids, target_ids))

        # Remove PowerProfiling if desired
        if not powerprofiling_enabled:
            root.remove(power_elem)
        else:
            self._logger.info("Enabled power profiling for %is" % (test_time_s - 60,))

        # Add XML items for actuation
        if gpio_actuations is not None:
            for node in node_ids:
                curr_actuations = gpio_actuations[gpio_actuations['node_id'] == node]
                if curr_actuations.shape[0] == 0:
                    continue

                actuation_elem = ET.SubElement(root, 'gpioActuationConf')
                ET.SubElement(actuation_elem, 'obsIds').text = str(node)

                for actuation in curr_actuations.itertuples(index=False):
                    pinConf = ET.SubElement(actuation_elem, 'pinConf')
                    ET.SubElement(pinConf, 'pin').text    = FL_ACTUATION_PIN
                    ET.SubElement(pinConf, 'level').text  = 'high'
                    ET.SubElement(pinConf, 'offset').text = str(actuation.offset_s)
                    ET.SubElement(pinConf, 'period').text = str(2 * actuation.duration_s)
                    ET.SubElement(pinConf, 'count').text  = str(1)

                    # nRST pin is not traced and hence not usable for post-processing; hence, we additionally toggle the standard FL_ACTUATION_PIN in parallel to visualize what is happening
                    # As 'toggle' does not work as expected for nRST, we need to manually set two actuations
                    if actuation.pin == FL_RESET_PIN:
                        pinConf = ET.SubElement(actuation_elem, 'pinConf')
                        ET.SubElement(pinConf, 'pin').text    = FL_RESET_PIN
                        ET.SubElement(pinConf, 'level').text  = 'low'
                        ET.SubElement(pinConf, 'offset').text = str(actuation.offset_s)

                        pinConf = ET.SubElement(actuation_elem, 'pinConf')
                        ET.SubElement(pinConf, 'pin').text    = FL_RESET_PIN
                        ET.SubElement(pinConf, 'level').text  = 'high'
                        ET.SubElement(pinConf, 'offset').text = str(actuation.offset_s + actuation.duration_s)

                self._logger.info('Included gpioActuationConf for node %2d with %2d actuation event%s' % (node, curr_actuations.shape[0], 's' if curr_actuations.shape[0] > 1 else '',))

        # Enable pretty printing of XML for better readability
        if pretty_print:
            self.xml_indent(root)

        # Write file
        xml_string = ET.tostring(root, encoding='unicode', method='xml')
        test_file = open(FILE_DIR + FL_XML_TEST, 'w')
        test_file.write(xml_string)
        test_file.close()

        self._logger.debug("Updated XML test configuration")

    def upload_xml(self, xml=FILE_DIR + FL_XML_TEST, meta_data=""):

        # Access fl-tools functions
        fl = flocklab.Flocklab()

        # Validate XML
        ret = fl.xmlValidate(xml)
        if 'validated correctly' not in ret:
            raise ValueError('Invalid FlockLab XML file: %s' % (ret,))
        else:
            self._logger.debug('XML validated correctly')

        # Upload XML
        ret = fl.createTestWithInfo(xml)
        if 'was successfully added' not in ret:
            raise ValueError('Could not upload XML to FlockLab: %s' % (ret,))
        else:
            self._logger.info(ret.replace('was successfully added and is scheduled to start', 'starts'))

            # Log test
            ret       = ret.split(' ')
            test_nr   = ret[1]
            test_date = ret[11] + ' ' + ret[12]
            self.log_test(','.join([test_nr, test_date, '\'' + meta_data + '\'']))

    def log_test(self, log_str="", path=FILE_DIR + FL_TEST_LOG):

        if not isinstance(log_str, str):
            self._logger.warning('Given item to log is not a string: %s' % (str(log_str),))
            return False

        log_file   = open(path, 'a+')
        str_to_log = dt.datetime.now().strftime(FL_TIME_FORMAT) + " - " + log_str + "\n"
        log_file.write(str_to_log)
        log_file.close()

    def get_logged_tests(self, path=FILE_DIR + FL_TEST_LOG):

        if path is None:
            path = FILE_DIR + FL_TEST_LOG

        if not os.path.isfile(path):
            self._logger.warning('Log file (%s) not found' % (path,))
            return None

        # Get logs
        log_file = open(path, 'r')
        data     = log_file.read()
        log_file.close()

        # Parse lines
        lines     = data.split('\n')
        rows_list = []

        for line in lines:

            # Make sure line is not empty or not commented out
            if len(line) == 0 or line[0] == '#' or line[:2] == '//':
                continue

            # Extract time of creation and logged data
            time_log, data = line.split(' - ')

            test_id, time_scheduled, meta_data = data.split(',', maxsplit=3)

            row = {'time_log':       dt.datetime.strptime(time_log,       FL_TIME_FORMAT),
                   'time_scheduled': dt.datetime.strptime(time_scheduled, FL_TIME_FORMAT),
                   'test_id':        int(test_id),
                   'meta_data':      literal_eval(meta_data) if len(meta_data) else None}
            rows_list.append(row)

            self._logger.debug('Read test %s from log, scheduled at %s' % (test_id, time_scheduled,))

        return pd.DataFrame(rows_list)

    def download_tests(self, log_path):

        # Get all locally scheduled tests
        logs = self.get_logged_tests(log_path)

        # Fetch corresponding tests
        fl                 = flocklab.Flocklab()
        test_ids           = logs['test_id']
        download_directory = FILE_DIR + FL_TEST_DIR

        if not os.path.isdir(download_directory):
            os.mkdir(download_directory)
            self._logger.info('Created new test directory (%s)' % (download_directory,))

        failed_tests = 0
        for test_id in test_ids:
            if isdir(download_directory + str(test_id)):
                self._logger.debug('Skipping test %d as already downloaded' % (test_id,))
            else:
                # Fetch test from FlockLab - catch FlockLab errors directly to not affect other tests
                try:
                    if 'Successfully downloaded & extracted:' in fl.getResults(test_id, outDir=download_directory):
                        self._logger.info('Successfully downloaded test %d' % (test_id,))
                    else:
                        self._logger.warning('Failed to download test %d' % (test_id,))

                except flocklab.flocklab.FlocklabError as exception:
                    failed_tests += 1
                    self._logger.warning('Experienced FlockLab error for test %d: %s' % (test_id, exception))

        if failed_tests == 0:
            self._logger.info('Downloaded data for all %d logged tests' % (len(test_ids),))
        else:
            self._logger.warning('Downloaded data for %d logged tests; %d attempts failed' % (len(test_ids) - failed_tests, failed_tests,))

    class SlotType(IntEnum):
        Proposal      = 1
        Voting        = 2
        Notification  = 3
        Scheduling    = 4
        Communication = 5
        Discovery     = 6

    def extract_metrics(self, test_id, path=FILE_DIR + FL_TEST_DIR):

        # Identify used protocol
        serial_path = path + '/' + str(test_id) + FL_SERIAL

        if not isfile(serial_path):
            self._logger.warning('Invalid path to Serial file: %s' % (serial_path,))
            return False

        # Read Serial file
        col_names = ['timestamp', 'observer_id', 'node_id', 'direction', 'output']
        serial_df = pd.read_csv(serial_path, names=col_names, skiprows=1, usecols=range(len(col_names)))

        # Identify protocol through 'Firmware' line
        firmware_ln = serial_df.loc[serial_df['output'].str.contains('Firmware', na=False), 'output']
        if firmware_ln.shape[0] > 0:
            if   'DPP2Demo' in firmware_ln.iloc[0]:
                return self.extract_demos_metrics(test_id, path=path)
            elif 'DPP2Hydr' in firmware_ln.iloc[0]:
                raise ValueError('Hydra metrics extraction not supported in this file')
            else:
                raise ValueError('Unknown protocol encountered with firmware \'%s\'' % firmware_ln.iloc[0])

        # Identify protocol by parsing first 100 lines of output
        head_df = serial_df.iloc[:100]
        if head_df['output'].str.contains('LWB', na=False).any():
            return self.extract_lwb_metrics(test_id, path=path)
        else:
            raise ValueError('Unknown protocol encountered')

    def extract_demos_metrics(self, test_id, path=FILE_DIR + FL_TEST_DIR, mobile_node=None):

        test_path   = path + '/' + str(test_id)
        gpio_path   = test_path + FL_GPIO
        serial_path = test_path + FL_SERIAL

        if not isdir(test_path):
            self._logger.warning('Invalid test directory: %s' % (test_path,))
            return False
        elif not isfile(gpio_path):
            self._logger.warning('Invalid path to GPIO tracing file: %s' % (gpio_path,))
            return False
        elif not isfile(serial_path):
            self._logger.warning('Invalid path to Serial file: %s' % (serial_path,))
            return False

        # Read Serial file - notice that the 'output' column might have missing data if commas have been used in the print if 'usecols=range(len(col_names))' is used as an option for 'read_csv'
        col_names = ['timestamp', 'observer_id', 'node_id', 'direction', 'output']
        serial_df = pd.read_csv(serial_path, names=col_names, skiprows=1)

        df = serial_df[['timestamp', 'node_id', 'output']].copy()
        df.set_index('timestamp', drop=False, inplace=True)
        df.sort_index(inplace=True)

        # Also add info from mobile node if available
        if mobile_node is not None:
            mobile_df               = pd.read_csv(test_path + FL_MOBILE_SERIAL, sep=']', names=['timestamp', 'output'])
            mobile_df['timestamp']  = mobile_df['timestamp'].str[len('['):].astype(float)      # Remove initial '[' and cast to int
            mobile_df['timestamp'] += df['timestamp'].min()                                    # Offset start time
            mobile_df['output']     = mobile_df['output'].str[len(' [90m'):-len('[0m')]  # Remove initial and ending characters for GTKTERM
            mobile_df['node_id']    = mobile_node
            df = pd.concat([df, mobile_df])
            df.set_index('timestamp', drop=False, inplace=True)
            df.sort_index(inplace=True)

        # Drop lines where the timestamp or the output does not exist (is nan)
        if df.index.isnull().any():
            self._logger.warning('Had to drop %3d serial lines due to missing timestamp' % np.count_nonzero(df.index.isnull()))
            df = df[df.index.notnull()]
        if df['output'].isnull().any():
            self._logger.info('Had to drop %3d serial lines due to missing output' % np.count_nonzero(df['output'].isnull()))
            df = df[df['output'].notnull()]

        # Convert timestamp to datetime object
        df['ts'] = pd.to_datetime(df['timestamp'], utc=False, unit='s')

        # Find statistics per node
        node_ids  = sorted(df['node_id'].unique())
        rows_list = []

        for node_id in node_ids:
            curr_serial = df.loc[df['node_id'] == node_id]

            # Go through each round and gather statistics
            eor_info    = curr_serial.loc[curr_serial['output'].str.contains('State at EoR'), 'output']
            round_start = 0
            curr_state  = 0
            curr_leader = 0

            for round_end, output in eor_info.items():
                # Store previous variables at the beginning of the round
                prev_state    = curr_state
                prev_leader   = curr_leader

                # Find next informational print on end-of-round
                eor_infos     = output[29:].split('/')
                curr_round    = int((eor_infos[0])[2:])
                curr_net_size = int((eor_infos[1])[2:])
                curr_state    = int((eor_infos[2])[2:])
                curr_demand   = int((eor_infos[3])[2:])
                has_voted     = int((eor_infos[4])[2:]) == 0
                curr_leader   = int((eor_infos[5])[2:])
                is_leader     = int((eor_infos[6])[2:]) == 1

                # Fetch all output from this round
                round_output = curr_serial.loc[round_start:round_end, 'output']

                # Extract P info
                p_nr_phases       = round_output[round_output.str.contains('Listening for P') | round_output.str.contains('P sent')].shape[0]
                p_proposals       = round_output[round_output.str.contains('P rcvd')          | round_output.str.contains('P sent')]
                p_nr_proposals    = p_proposals.shape[0]
                p_leader_proposed = bool(p_proposals.str.contains('in phase 1').any())

                # Extract V info
                v_counter_ln = round_output[round_output.str.contains('Completed in slot')]
                v_activity   = round_output[round_output.str.contains('Rcvd in')]
                v_votes      = round_output[round_output.str.contains('Votes collected')]
                v_counter    = 0 if (v_counter_ln.shape[0] > 0) else -1
                v_rx_success = 0 if (v_activity.shape[0]   > 0) else -1
                v_rx_perc    = 0 if (v_activity.shape[0]   > 0) else -1
                v_rx_done    = 0 if (v_activity.shape[0]   > 0) else -1
                v_tx_success = 0 if (v_activity.shape[0]   > 0) else -1
                v_nr_votes   = 0 if (v_votes.shape[0]      > 0) else -1

                for index, row in v_counter_ln.items():
                    v_counter += int(row[31:])
                v_counter /= v_counter_ln.shape[0] if (v_counter_ln.shape[0] > 0) else 1

                for index, row in v_activity.items():
                    curr_rx_success  = int(row[32:34])
                    v_rx_success    += curr_rx_success
                    curr_rx_perc     = int(row[42:45])
                    v_rx_perc       += curr_rx_perc
                    v_rx_done       += round(curr_rx_success * 100 / curr_rx_perc) if (curr_rx_perc > 0) else 0
                    v_tx_success    += int(row[60:62])
                v_rx_success /= v_activity.shape[0] if (v_activity.shape[0] > 0) else 1
                v_rx_perc    /= v_activity.shape[0] if (v_activity.shape[0] > 0) else 1
                v_rx_done    /= v_activity.shape[0] if (v_activity.shape[0] > 0) else 1
                v_tx_success /= v_activity.shape[0] if (v_activity.shape[0] > 0) else 1

                for index, row in v_votes.items():
                    v_nr_votes += row.replace(" | ", "").split()[-1].count('1')
                v_nr_votes /= v_votes.shape[0] if (v_votes.shape[0] > 0) else 1

                v_vote_ln    = round_output[round_output.str.contains('Voted in')]
                v_vote_phase = int(v_vote_ln.iloc[0][30:31]) if (v_vote_ln.shape[0] > 0) else -1
                v_vote_id    = int(v_vote_ln.iloc[0][36:38]) if (v_vote_ln.shape[0] > 0) else 0

                if (v_vote_id > 0) != has_voted:
                    self._logger.warning("Expected vote to be %u, but %2u voted for %2u in round %3u" % (has_voted, node_id, v_vote_id, curr_round))

                # Extract N info
                n_notification_ln   = round_output[round_output.str.contains('notification: N')]
                n_notification_rcvd = int(n_notification_ln.iloc[0][32:]) if n_notification_ln.str.contains('N rcvd').any() else -1
                n_notification_sent = int(n_notification_ln.iloc[0][39:]) if n_notification_ln.str.contains('N sent').any() else -1

                # Extract S info

                # Used S for scheduling
                s_schedule_ln = round_output[round_output.str.contains('S rcvd')]
                s_version_ln  = round_output[round_output.str.contains('S for round')]
                s_version_ln  = s_version_ln[~s_version_ln.str.contains('not rcvd')].str.rsplit(expand=True)
                s_leader_id   = int(s_schedule_ln.iloc[0][-2:])   if (s_schedule_ln.shape[0] == 1) else 0
                s_sched_hash  = int(s_version_ln.iloc[0, -1], 16) if (s_version_ln.shape[0]  == 1)  else -1

                # Adjust if it is the leader itself
                s_schedule_ln = round_output[round_output.str.contains('S sent')]
                if s_schedule_ln.shape[0] > 0:
                    s_leader_id = node_id

                # Used S for notification
                s_notification_ln = round_output[round_output.str.contains('N rcvd')]
                s_notification_id = int(s_notification_ln.iloc[0][-2:]) if (s_notification_ln.shape[0] == 1) else 0

                # Adjust if it is the leader itself
                s_notification_ln = round_output[round_output.str.contains('N sent')]
                if s_notification_ln.shape[0] > 0:
                    s_notification_id = int(s_notification_ln.iloc[0][-2:])

                # Extract C info
                rx_info     = round_output[round_output.str.contains('C rcvd')].str.rsplit(expand=True)
                pkts_rcvd   = rx_info.shape[0]
                pkts_missed = round_output[round_output.str.contains('C missed')].shape[0]
                pkts_sent   = round_output[round_output.str.contains('C sent')  ].shape[0]
                pkts_total  = pkts_rcvd + pkts_missed + pkts_sent

                if pkts_rcvd > 0:
                    hop_info   = rx_info.iloc[:, 11].astype(int)
                    rx_hop_avg = hop_info.mean()
                    rx_hop_max = hop_info.max()
                else:
                    # Replace with NaN during analysis
                    rx_hop_avg = -1
                    rx_hop_max = -1

                # Extract D info
                d_version_ln = round_output[round_output.str.contains('Generated Discovery S')].str.rsplit(expand=True)
                d_ignored_ln = round_output[round_output.str.contains('D ignored')]
                d_sched_hash = int(d_version_ln.iloc[0, 5], 16) if (d_version_ln.shape[0] == 1) else -1
                d_accepted   = round_output[round_output.str.contains('D rcvd')].shape[0]
                d_inferior   = d_ignored_ln[d_ignored_ln.str.contains('<')].shape[0]
                d_ignored    = d_ignored_ln.shape[0] - d_inferior

                # Extract duty cycle info - 'prev_state' permits filtering unwanted influences (e.g. bootstrapping) explicitly
                rx_dc_ln  = round_output[round_output.str.contains('Rx duty cycle')]
                rx_dc     = rx_dc_ln.iloc[0].split()[-1].split('/')  if (rx_dc_ln.shape[0] == 1) else ['-1ppm', '-1ppm', '-1ppm', '-1ppm', '-1ppm', '-1ppm']
                rx_dc_p   = int(rx_dc[0][:-3]) * 1e-06
                rx_dc_v   = int(rx_dc[1][:-3]) * 1e-06
                rx_dc_n   = int(rx_dc[2][:-3]) * 1e-06
                rx_dc_s   = int(rx_dc[3][:-3]) * 1e-06
                rx_dc_c   = int(rx_dc[4][:-3]) * 1e-06
                rx_dc_d   = int(rx_dc[5][:-3]) * 1e-06
                tx_dc_ln  = round_output[round_output.str.contains('Tx duty cycle')]
                tx_dc     = tx_dc_ln.iloc[0].split()[-1].split('/')  if (tx_dc_ln.shape[0] == 1) else ['-1ppm', '-1ppm', '-1ppm', '-1ppm', '-1ppm', '-1ppm']
                tx_dc_p   = int(tx_dc[0][:-3]) * 1e-06
                tx_dc_v   = int(tx_dc[1][:-3]) * 1e-06
                tx_dc_n   = int(tx_dc[2][:-3]) * 1e-06
                tx_dc_s   = int(tx_dc[3][:-3]) * 1e-06
                tx_dc_c   = int(tx_dc[4][:-3]) * 1e-06
                tx_dc_d   = int(tx_dc[5][:-3]) * 1e-06
                cpu_dc_ln = round_output[round_output.str.contains('DC - ')]
                cpu_dc    = float(cpu_dc_ln.iloc[0].split()[-1].split('/')[0][:-1]) * 1e-02 if (cpu_dc_ln.shape[0] == 1) else -1

                row = {'timestamp': round_end, 'test_id': test_id, 'node_id': node_id, 'round': curr_round, 'nr_phases': p_nr_phases, 'prev_state': prev_state, 'curr_state': curr_state,
                       'demand': curr_demand, 'network_size': curr_net_size, 'prev_leader': prev_leader, 'curr_leader': curr_leader, 'is_leader': is_leader, 'voted': has_voted,
                       'p_success': p_nr_proposals / p_nr_phases if (p_nr_phases > 0) else 0, 'p_leader_proposed': p_leader_proposed,
                       'v_success': v_counter >= 0, 'v_counter': v_counter, 'v_nr_votes': v_nr_votes, 'v_vote_id': v_vote_id, 'v_vote_phase': v_vote_phase,
                       'v_rx_success': v_rx_success, 'v_rx_done': v_rx_done, 'v_tx_success': v_tx_success,
                       'n_notification_sent': n_notification_sent, 'n_notification_rcvd': n_notification_rcvd,
                       's_leader_id': s_leader_id, 's_sched_hash': s_sched_hash, 's_notification_id': s_notification_id,
                       'pkts_rcvd': pkts_rcvd, 'pkts_missed': pkts_missed, 'pkts_sent': pkts_sent, 'nr_hops_avg': rx_hop_avg, 'nr_hops_max': rx_hop_max,
                       'd_sched_hash': d_sched_hash, 'd_accepted': d_accepted, 'd_inferior': d_inferior, 'd_ignored': d_ignored,
                       'rx_dc_p': rx_dc_p, 'rx_dc_v': rx_dc_v, 'rx_dc_n': rx_dc_n, 'rx_dc_s': rx_dc_s, 'rx_dc_c': rx_dc_c, 'rx_dc_d': rx_dc_d,
                       'tx_dc_p': tx_dc_p, 'tx_dc_v': tx_dc_v, 'tx_dc_n': tx_dc_n, 'tx_dc_s': tx_dc_s, 'tx_dc_c': tx_dc_c, 'tx_dc_d': tx_dc_d,
                       'cpu_dc': cpu_dc}
                rows_list.append(row)

                round_start = round_end

        statistics_node = pd.DataFrame(rows_list)

        # Find network statistics
        stats_copy = statistics_node.copy()
        stats_copy.set_index('timestamp', drop=False, inplace=True)
        stats_copy.sort_index(inplace=True)

        # Extract Tx power
        radio_config = df.loc[df['output'].str.contains('Radio config'), 'output']
        tx_power     = int(radio_config.iloc[0].split()[-1][:-len('dBm')])

        rows_list    = []
        nr_phases    = stats_copy['nr_phases'].max()
        curr_time    = stats_copy.index.min()
        search_scope = 3  # How long to search for prints after the given event [s]

        # Note: Do not use rounds, as they are not synchronized
        # curr_round = stats_copy['round'].min()
        # while curr_round <= stats_copy['round'].max():
        #     curr_stats = stats_copy[stats_copy['round'] == curr_round]
        #     curr_round += 1

        while curr_time <= stats_copy.index.max():
            curr_stats = stats_copy[curr_time:(curr_time + search_scope)]
            curr_round = curr_stats['round'].median()

            if curr_stats.shape[0] != len(node_ids):
                self._logger.debug('Expected %2i entries per round, but observed %2i in current interval around round %3u' % (len(node_ids), curr_stats.shape[0], curr_round,))

            # Votes
            candidates = []
            votes_phase = dict.fromkeys([phase   for phase   in range(1, nr_phases + 1)],       0)
            votes_node  = dict.fromkeys([node_id for node_id in set(node_ids + FL_MOBILE_IDS)], 0)
            for stats in curr_stats.itertuples():
                if stats.v_vote_id > 0:
                    candidates.append(stats.v_vote_id)
                    votes_phase[stats.v_vote_phase] = votes_phase[stats.v_vote_phase] + 1
                    votes_node[stats.v_vote_id]     = votes_node[stats.v_vote_id]     + 1

            if sum(votes_phase.values()) != len(node_ids):
                self._logger.debug('Collected %2u casted votes with %2u abstentions around round %3u' % (sum(votes_phase.values()), len(node_ids) - sum(votes_phase.values()), curr_round,))

            # Leaders and schedules
            leaders     = []
            s_schedules = []
            d_schedules = []
            for stats in curr_stats.itertuples():
                if stats.s_leader_id > 0:
                    leaders.append(stats.s_leader_id)
                if stats.s_sched_hash > 0:
                    s_schedules.append(stats.s_sched_hash)
                if stats.d_sched_hash > 0:
                    d_schedules.append(stats.d_sched_hash)

            # Network parameters
            network_diam     = curr_stats['nr_hops_max'].max()
            network_size_avg = curr_stats['network_size'].median()
            network_size_min = curr_stats['network_size'].min()
            network_size_max = curr_stats['network_size'].max()

            row = {'timestamp': curr_stats['timestamp'].median(), 'test_id': test_id, 'round': curr_round,
                   'N_avg': network_size_avg, 'N_min': network_size_min, 'N_max': network_size_max,
                   'candidates': len(set(candidates)), 'votes_phase': votes_phase, 'votes_node': votes_node,
                   'leaders': len(set(leaders)), 's_scheds': len(set(s_schedules)), 'd_scheds': len(set(d_schedules)),
                   'n_sent': (curr_stats['n_notification_sent'] > 0).sum(), 'n_rcvd': (curr_stats['n_notification_rcvd'] > 0).sum(),
                   'd_accepted': curr_stats['d_accepted'].sum(), 'd_inferior': curr_stats['d_inferior'].sum(), 'd_ignored': curr_stats['d_ignored'].sum(),
                   'tx_power': tx_power, 'network_diam': network_diam}
            rows_list.append(row)

            curr_time = stats_copy[(curr_time + search_scope):].index.min()

        statistics_network = pd.DataFrame(rows_list)

        # Find log statistics
        nr_warnings = df['output'].str.contains('WARN',  na=False).sum()
        nr_errors   = df['output'].str.contains('ERROR', na=False).sum()


        # Read GPIO file
        col_names = ['timestamp', 'observer_id', 'node_id', 'pin_name', 'value']
        gpio_df   = pd.read_csv(gpio_path, names=col_names, skiprows=1, float_precision='round_trip')

        df = gpio_df[['timestamp', 'observer_id', 'node_id', 'pin_name', 'value']].copy()
        df.set_index('timestamp', drop=False, inplace=True)
        df.sort_index(inplace=True)

        # Convert timestamp to datetime object
        df['ts'] = pd.to_datetime(df['timestamp'], utc=False, unit='s')

        # Filter unused pins
        used_pins = FL_TRACING_PINS
        df        = df[df['pin_name'].isin(used_pins)]

        # Filter initial values where each node is set at t=0
        df = df[(df.first_valid_index() + 0.000001):]

        # Count number of unique nodes (more reliable, as also valid when Serial did not work)
        tracing_ids = sorted(df['node_id'].unique())

        statistics_accuracy = None

        return [statistics_node, statistics_network, statistics_accuracy, tracing_ids, nr_warnings, nr_errors]

    def extract_lwb_metrics(self, test_id, path=FILE_DIR + FL_TEST_DIR):

        test_path   = path + '/' + str(test_id)
        gpio_path   = test_path + FL_GPIO
        serial_path = test_path + FL_SERIAL

        if not isdir(test_path):
            self._logger.warning('Invalid test directory: %s' % (test_path,))
            return False
        elif not isfile(gpio_path):
            self._logger.warning('Invalid path to GPIO tracing file: %s' % (gpio_path,))
            return False
        elif not isfile(serial_path):
            self._logger.warning('Invalid path to Serial file: %s' % (serial_path,))
            return False

        # Read Serial file - notice that the 'output' column might have missing data if commas have been used in the print if 'usecols=range(len(col_names))' is used as an option for 'read_csv'
        col_names = ['timestamp', 'observer_id', 'node_id', 'direction', 'output']
        serial_df = pd.read_csv(serial_path, names=col_names, skiprows=1, usecols=range(len(col_names)))

        df = serial_df[['timestamp', 'node_id', 'output']].copy()
        df.set_index('timestamp', drop=False, inplace=True)
        df.sort_index(inplace=True)

        # Drop lines where the timestamp or the output does not exist (is nan)
        if df.index.isnull().any():
            self._logger.warning('Had to drop %3d serial lines due to missing timestamp' % np.count_nonzero(df.index.isnull()))
            df = df[df.index.notnull()]
        if df['output'].isnull().any():
            self._logger.info('Had to drop %3d serial lines due to missing output' % np.count_nonzero(df['output'].isnull()))
            df = df[df['output'].notnull()]

        # Convert timestamp to datetime object
        df['ts'] = pd.to_datetime(df['timestamp'], utc=False, unit='s')

        # Find statistics per node
        node_ids  = sorted(df['node_id'].unique())
        rows_list = []

        for node_id in node_ids:
            curr_serial = df.loc[df['node_id'] == node_id]

            # Go through each round and gather statistics
            dc_info     = curr_serial.loc[curr_serial['output'].str.contains('task_post: DC - '), 'output']
            round_start = 0

            for index, output in dc_info.items():
                # Find CPU duty cycle
                cpu_dc_ln = output
                cpu_dc    = float(cpu_dc_ln.split(';')[-1].split()[-1][:-1]) * 1e-02

                # Fetch all output from this round (some is potentially printed after the DC print)
                buffer_s     = 0.1
                round_output = curr_serial.loc[round_start:(index + buffer_s), 'output']

                # Extract duty cycle info
                rx_dc_ln = round_output[round_output.str.contains('Rx duty cycle')]
                rx_dc    = rx_dc_ln.iloc[0].split()[-1].split('/')
                rx_dc_c  = int(rx_dc[0][:-3]) * 1e-06
                rx_dc_v  = int(rx_dc[1][:-3]) * 1e-06
                rx_dc_s  = int(rx_dc[2][:-3]) * 1e-06
                tx_dc_ln = round_output[round_output.str.contains('Tx duty cycle')]
                tx_dc    = tx_dc_ln.iloc[0].split()[-1].split('/')
                tx_dc_c  = int(tx_dc[0][:-3]) * 1e-06
                tx_dc_v  = int(tx_dc[1][:-3]) * 1e-06
                tx_dc_s  = int(tx_dc[2][:-3]) * 1e-06

                # Mark round if node was synced
                node_synced = round_output.str.contains(' SYN ').any()

                # Get packet statistics
                pkts_received = round_output[round_output.str.contains('Rcvd   pkt')].shape[0]
                pkts_missed   = round_output[round_output.str.contains('Missed pkt')].shape[0]
                pkts_sent     = round_output[round_output.str.contains('Pkt sent')  ].shape[0]
                pkts_total    = pkts_received + pkts_missed + pkts_sent

                row = {'timestamp': index, 'test_id': test_id, 'node_id': node_id, 'node_synced': node_synced,
                       'pkts_rcvd': pkts_received, 'pkts_missed': pkts_missed, 'pkts_sent': pkts_sent,
                       'rx_dc_c': rx_dc_c, 'rx_dc_v': rx_dc_v, 'rx_dc_s': rx_dc_s, 'tx_dc_c': tx_dc_c, 'tx_dc_v': tx_dc_v, 'tx_dc_s': tx_dc_s, 'cpu_dc': cpu_dc}
                rows_list.append(row)

                round_start = index + buffer_s

        statistics_node = pd.DataFrame(rows_list)

        # Find log statistics
        nr_warnings = df['output'].str.contains('WARN',  na=False).sum()
        nr_errors   = df['output'].str.contains('ERROR', na=False).sum()


        # Read GPIO file
        col_names = ['timestamp', 'observer_id', 'node_id', 'pin_name', 'value']
        gpio_df   = pd.read_csv(gpio_path, names=col_names, skiprows=1, float_precision='round_trip')

        df = gpio_df[['timestamp', 'observer_id', 'node_id', 'pin_name', 'value']].copy()
        df.set_index('timestamp', drop=False, inplace=True)
        df.sort_index(inplace=True)

        # Convert timestamp to datetime object
        df['ts'] = pd.to_datetime(df['timestamp'], utc=False, unit='s')

        # Filter unused pins
        used_pins = FL_TRACING_PINS
        df        = df[df['pin_name'].isin(used_pins)]

        # Filter initial values where each node is set at t=0
        df = df[(df.first_valid_index() + 0.000001):]

        # Count number of unique nodes (more reliable, as also valid when Serial did not work)
        tracing_ids = sorted(df['node_id'].unique())

        return [statistics_node, None, None, tracing_ids, nr_warnings, nr_errors]

    def store_metrics(self, log_path=FILE_DIR):

        # Get all locally scheduled tests
        logs = self.get_logged_tests(log_path)

        # Extract and store metrics for all tests
        test_ids       = logs['test_id']
        test_directory = FILE_DIR + FL_TEST_DIR

        metrics_prev = 0
        metrics_new  = 0
        for test_id in test_ids:
            if not isdir(test_directory + str(test_id)):
                self._logger.warning('Skipping test %d as not yet downloaded' % (test_id,))
            elif sum([ (('/' + f).find(FL_STATS_NODES) > -1) for f in os.listdir(test_directory + str(test_id))]) > 0:
                metrics_prev += 1
                self._logger.debug('Skipping test %d as already extracted metrics' % (test_id,))
            else:
                # Get metrics
                metrics = self.extract_metrics(test_id)

                # Print errors and warnings
                if metrics[5] > 0:
                    self._logger.warning('Test %d had %d errors and %d warnings for %2d nodes' % (test_id, metrics[5], metrics[4], len(metrics[3]),))
                else:
                    self._logger.info('Test %d had %d warnings for %2d nodes' % (test_id, metrics[4], len(metrics[3]),))

                # Store statistics as pickle files
                appendix = '_n_%d_test_%d.pkl' % (len(metrics[3]), test_id,)
                if metrics[0] is not None:
                    metrics[0].to_pickle(test_directory + str(test_id) + FL_STATS_NODES   + appendix)
                if metrics[1] is not None:
                    metrics[1].to_pickle(test_directory + str(test_id) + FL_STATS_NETWORK + appendix)
                if metrics[2] is not None:
                    metrics[2].to_pickle(test_directory + str(test_id) + FL_STATS_ACCUR + appendix)

                metrics_new += 1
                self._logger.info('Stored test metrics of test %d' % (test_id,))

        self._logger.info('Stored %d additional test metrics for a total of %d stored metrics' % (metrics_new, metrics_new + metrics_prev,))

    def get_metrics(self, test_id, path=FILE_DIR + FL_TEST_DIR):
        test_directory = path + str(test_id)

        if not isdir(test_directory):
            self._logger.warning('Skipping test %d as not yet downloaded' % (test_id,))
            return None
        elif sum([(('/' + f).find(FL_STATS_NODES) > -1) for f in os.listdir(test_directory)]) == 0:
            self._logger.warning('Skipping test %d as not extracted yet' % (test_id,))
            return None
        else:
            # Get metrics
            statistics_node     = pd.DataFrame()
            statistics_network  = pd.DataFrame()
            statistics_accuracy = None

            for f in os.listdir(test_directory):
                file_path = test_directory + '/' + f
                if   FL_STATS_NODES   in file_path:
                    statistics_node     = pd.read_pickle(file_path)
                elif FL_STATS_NETWORK in file_path:
                    statistics_network  = pd.read_pickle(file_path)
                elif FL_STATS_ACCUR   in file_path:
                    statistics_accuracy = pd.read_pickle(file_path)

            return [statistics_node, statistics_network, statistics_accuracy]

    def get_state_trace(self, test_id, path=FILE_DIR + FL_TEST_DIR, drop_affected=False, filter_ids=None, mobile_node=None, accept_logs_only=False):

        # Get traces
        [states_df, affected_nodes] = self.extract_state_trace(test_id, path, mobile_node=mobile_node, accept_missing_mobile=accept_logs_only)

        if drop_affected:
            filter_ids = affected_nodes if filter_ids is None else affected_nodes | set(filter_ids)

        # Get log stats
        [node_df, network_df, nr_warnings, nr_errors] = self.extract_state_logs(test_id, path, filter_ids=filter_ids, mobile_node=mobile_node)
        if nr_errors > 0:
            self._logger.warning('Extracted state from test %u, which had %u errors and %u warnings' % (test_id, nr_errors, nr_warnings,))
        else:
            self._logger.info('Extracted state from test %u, which had %u warnings' % (test_id, nr_warnings,))

        return [states_df, node_df, network_df]

    class State(IntEnum):
        UNKNOWN     = 0
        BOOT        = 1
        SYNCED      = 2
        SCHEDULED   = 3
        ACTUATED    = 4

    def extract_state_trace(self, test_id, path=FILE_DIR + FL_TEST_DIR, mobile_node=None, accept_missing_mobile=False):

        test_path = path + '/' + str(test_id)
        gpio_path = test_path + FL_GPIO

        if not isdir(test_path):
            self._logger.warning('Invalid test directory: %s' % (test_path,))
            return False
        elif not isfile(gpio_path):
            self._logger.warning('Invalid path to GPIO tracing file: %s' % (gpio_path,))
            return False


        # Read GPIO file
        col_names = ['timestamp', 'observer_id', 'node_id', 'pin_name', 'value']
        gpio_df   = pd.read_csv(gpio_path, names=col_names, skiprows=1, float_precision='round_trip')

        df = gpio_df[['timestamp', 'observer_id', 'node_id', 'pin_name', 'value']].copy()
        df.set_index('timestamp', drop=False, inplace=True)
        df.sort_index(inplace=True)

        if mobile_node is not None:
            if isfile(test_path + FL_MOBILE_GPIO):
                mobile_df               = pd.read_csv(test_path + FL_MOBILE_GPIO, names=col_names, skiprows=1, float_precision='round_trip')
                mobile_df['timestamp'] += df['timestamp'].min()

                df = pd.concat([df, mobile_df])
                df.set_index('timestamp', drop=False, inplace=True)
                df.sort_index(inplace=True)
            elif not accept_missing_mobile:
                raise ValueError('Missing GPIO file for test %u' % (test_id,))

        # Filter unused pins
        FL_STATE_PINS = ['INT1', 'INT2', 'SIG1']
        used_pins     = FL_STATE_PINS
        df            = df[df['pin_name'].isin(used_pins)]

        # Filter initial values where each node is set at t=0
        df = df[(df.first_valid_index() + 0.000001):]


        # Generate states trace
        node_ids       = sorted(df['node_id'].unique())
        affected_nodes = sorted(df.loc[df['pin_name'] == FL_ACTUATION_PIN, 'node_id'].unique())
        rows_list      = []

        for node_id in node_ids:
            curr_trace     = df[df['node_id'] == node_id]
            search_scope_s = 0.1

            curr_state = self.State.UNKNOWN
            start_s    = 0

            curr_time = curr_trace['timestamp'].min()
            end_time  = curr_trace['timestamp'].max()

            while curr_time <= end_time:

                # Get current state pins
                curr_state_pins = curr_trace[curr_time:(curr_time + search_scope_s)]

                # Analyse state which just finished
                if start_s > 0:
                    # Finish previous state
                    row = {'test_id': test_id, 'node_id': node_id, 'start_s': start_s, 'duration_s': curr_time - start_s, 'end_s': curr_time, 'state': curr_state}
                    rows_list.append(row)

                # Check if done
                if curr_time == end_time:
                    break

                start_s   = curr_time
                curr_time = curr_trace.loc[(curr_time + search_scope_s):, 'timestamp'].min()

                # Check actuation
                for actuation in curr_state_pins[curr_state_pins['pin_name'] == FL_ACTUATION_PIN].itertuples(index=False):
                    if actuation.value == 1:
                        curr_state += self.State.ACTUATED

                # Check INT1
                for actuation in curr_state_pins[curr_state_pins['pin_name'] == 'INT1'          ].itertuples(index=False):
                    if actuation.value == 1:
                        curr_state += 1
                    elif actuation.value == 0:
                        curr_state -= 1

                # Check INT2
                for actuation in curr_state_pins[curr_state_pins['pin_name'] == 'INT2'          ].itertuples(index=False):
                    if actuation.value == 1:
                        curr_state += 2
                    elif actuation.value == 0:
                        curr_state -= 2

        return [pd.DataFrame(rows_list), affected_nodes]

    def extract_state_logs(self, test_id, path=FILE_DIR + FL_TEST_DIR, filter_ids=None, mobile_node=None):

        test_path   = path + '/' + str(test_id)
        serial_path = test_path + FL_SERIAL

        if not isdir(test_path):
            self._logger.warning('Invalid test directory: %s' % (test_path,))
            return False
        elif not isfile(serial_path):
            self._logger.warning('Invalid path to Serial file: %s' % (serial_path,))
            return False

        # Read Serial file
        col_names = ['timestamp', 'observer_id', 'node_id', 'direction', 'output']
        serial_df = pd.read_csv(serial_path, names=col_names, skiprows=1, usecols=range(len(col_names)))

        df = serial_df[['timestamp', 'node_id', 'output']].copy()
        df.set_index('timestamp', drop=False, inplace=True)
        df.sort_index(inplace=True)

        # Drop lines where the timestamp or the output does not exist (is nan)
        if df.index.isnull().any():
            self._logger.warning('Had to drop %3d serial lines due to missing timestamp' % np.count_nonzero(df.index.isnull()))
            df = df[df.index.notnull()]
        if df['output'].isnull().any():
            self._logger.info('Had to drop %3d serial lines due to missing output' % np.count_nonzero(df['output'].isnull()))
            df = df[df['output'].notnull()]

        # Find log statistics
        nr_warnings = df['output'].str.contains('WARN',  na=False).sum()
        nr_errors   = df['output'].str.contains('ERROR', na=False).sum()

        # Identify protocol through 'Firmware' line
        firmware_ln = df.loc[df['output'].str.contains('Firmware', na=False), 'output']
        if firmware_ln.shape[0] > 0:
            if 'DPP2Demo' in firmware_ln.iloc[0]:
                [node_df, network_df] = self.extract_demos_logs(test_id, path, filter_ids=filter_ids, mobile_node=mobile_node)
                return [node_df, network_df, nr_warnings, nr_errors]
            else:
                raise ValueError('Unknown protocol encountered with firmware \'%s\'' % firmware_ln.iloc[0])

        # Identify protocol by parsing first 100 lines of output
        head_df = serial_df.iloc[:100]
        if head_df['output'].str.contains('LWB', na=False).any():
            [node_df, network_df] = self.extract_lwb_logs(test_id, path, filter_ids=filter_ids, mobile_node=mobile_node)
            return [node_df, network_df, nr_warnings, nr_errors]
        else:
            raise ValueError('Unknown protocol encountered')

    def extract_demos_logs(self, test_id, path=FILE_DIR + FL_TEST_DIR, filter_ids=None, fixed_stats_interval=True, mobile_node=None):

        test_path   = path + '/' + str(test_id)
        serial_path = test_path + FL_SERIAL

        if not isdir(test_path):
            self._logger.warning('Invalid test directory: %s' % (test_path,))
            return False
        elif not isfile(serial_path):
            self._logger.warning('Invalid path to Serial file: %s' % (serial_path,))
            return False

        # Read Serial file
        col_names = ['timestamp', 'observer_id', 'node_id', 'direction', 'output']
        serial_df = pd.read_csv(serial_path, names=col_names, skiprows=1, usecols=range(len(col_names)))

        df = serial_df[['timestamp', 'node_id', 'output']].copy()
        df.set_index('timestamp', drop=False, inplace=True)
        df.sort_index(inplace=True)

        # Also add info from mobile node if available
        if mobile_node is not None:
            mobile_df               = pd.read_csv(test_path + FL_MOBILE_SERIAL, sep=']', names=['timestamp', 'output'])
            mobile_df['timestamp']  = mobile_df['timestamp'].str[len('['):].astype(float)      # Remove initial '[' and cast to int
            mobile_df['timestamp'] += df['timestamp'].min()                                    # Offset start time
            mobile_df['output']     = mobile_df['output'].str[len(' [90m'):-len('[0m')]  # Remove initial and ending characters for GTKTERM
            mobile_df['node_id']    = mobile_node
            df = pd.concat([df, mobile_df])
            df.set_index('timestamp', drop=False, inplace=True)
            df.sort_index(inplace=True)

        # Find statistics per node
        node_ids  = sorted(df['node_id'].unique())
        rows_list = []

        for node_id in node_ids:
            curr_serial = df.loc[df['node_id'] == node_id]

            # Go through each round and gather statistics
            eor_info    = curr_serial.loc[curr_serial['output'].str.contains('State at EoR'), 'output']
            round_start = 0

            for round_end, output in eor_info.items():
                # Find next informational print on end-of-round
                eor_infos     = output[29:].split('/')
                curr_round    = int((eor_infos[0])[2:])
                curr_net_size = int((eor_infos[1])[2:])
                curr_state    = int((eor_infos[2])[2:])
                curr_demand   = int((eor_infos[3])[2:])
                has_voted     = int((eor_infos[4])[2:]) == 0
                curr_leader   = int((eor_infos[5])[2:])
                is_leader     = int((eor_infos[6])[2:]) == 1

                # Fetch all output from this round
                round_output = curr_serial.loc[round_start:round_end, 'output']

                # Extract P info
                p_proposals       = round_output[round_output.str.contains('P rcvd') | round_output.str.contains('P sent')]
                p_leader_proposed = bool(p_proposals.str.contains('in phase 1').any())

                # Extract V info
                v_counter_ln = round_output[round_output.str.contains('Completed in slot')]
                v_activity   = round_output[round_output.str.contains('Rcvd in')]
                v_votes      = round_output[round_output.str.contains('Votes collected')]
                v_counter    = 0 if (v_counter_ln.shape[0] > 0) else -1
                v_rx_success = 0 if (v_activity.shape[0]   > 0) else -1
                v_rx_perc    = 0 if (v_activity.shape[0]   > 0) else -1
                v_rx_done    = 0 if (v_activity.shape[0]   > 0) else -1
                v_tx_success = 0 if (v_activity.shape[0]   > 0) else -1
                v_nr_votes   = 0 if (v_votes.shape[0]      > 0) else -1

                for index, row in v_counter_ln.items():
                    v_counter += int(row[31:])
                v_counter /= v_counter_ln.shape[0] if (v_counter_ln.shape[0] > 0) else 1

                for index, row in v_activity.items():
                    curr_rx_success  = int(row[32:34])
                    v_rx_success    += curr_rx_success
                    curr_rx_perc     = int(row[42:45])
                    v_rx_perc       += curr_rx_perc
                    v_rx_done       += round(curr_rx_success * 100 / curr_rx_perc) if (curr_rx_perc > 0) else 0
                    v_tx_success    += int(row[60:62])
                v_rx_success /= v_activity.shape[0] if (v_activity.shape[0] > 0) else 1
                v_rx_perc    /= v_activity.shape[0] if (v_activity.shape[0] > 0) else 1
                v_rx_done    /= v_activity.shape[0] if (v_activity.shape[0] > 0) else 1
                v_tx_success /= v_activity.shape[0] if (v_activity.shape[0] > 0) else 1

                for index, row in v_votes.items():
                    v_nr_votes += row.replace(" | ", "").split()[-1].count('1')
                v_nr_votes /= v_votes.shape[0] if (v_votes.shape[0] > 0) else 1

                v_vote_ln    = round_output[round_output.str.contains('Voted in')]
                v_vote_phase = int(v_vote_ln.iloc[0][30:31]) if (v_vote_ln.shape[0] > 0) else -1
                v_vote_id    = int(v_vote_ln.iloc[0][36:38]) if (v_vote_ln.shape[0] > 0) else 0

                # Extract N info
                n_notification_ln   = round_output[round_output.str.contains('notification: N')]
                n_notification_rcvd = int(n_notification_ln.iloc[0][32:]) if n_notification_ln.str.contains('N rcvd').any() else -1
                n_notification_sent = int(n_notification_ln.iloc[0][39:]) if n_notification_ln.str.contains('N sent').any() else -1

                # Extract S info

                # Used S for scheduling
                s_schedule_ln = round_output[round_output.str.contains('S rcvd')]
                s_version_ln  = round_output[round_output.str.contains('S for round')]
                s_version_ln  = s_version_ln[~s_version_ln.str.contains('not rcvd')].str.rsplit(expand=True)
                s_leader_id   = int(s_schedule_ln.iloc[0][-2:])   if (s_schedule_ln.shape[0] == 1) else 0
                s_sched_hash  = int(s_version_ln.iloc[0, -1], 16) if (s_version_ln.shape[0]  == 1) else -1

                # Adjust if it is the leader itself
                s_schedule_ln = round_output[round_output.str.contains('S sent')]
                if s_schedule_ln.shape[0] > 0:
                    s_leader_id = node_id

                # Used S for notification
                s_notification_ln = round_output[round_output.str.contains('N rcvd')]
                s_notification_id = int(s_notification_ln.iloc[0][-2:]) if (s_notification_ln.shape[0] == 1) else 0

                # Adjust if it is the leader itself
                s_notification_ln = round_output[round_output.str.contains('N sent')]
                if s_notification_ln.shape[0] > 0:
                    s_notification_id = int(s_notification_ln.iloc[0][-2:])

                # Extract C info
                rx_info     = round_output[round_output.str.contains('C rcvd')].str.rsplit(expand=True)
                pkts_rcvd   = rx_info.shape[0]
                pkts_missed = round_output[round_output.str.contains('C missed')].shape[0]
                pkts_sent   = round_output[round_output.str.contains('C sent')  ].shape[0]
                pkts_total  = pkts_rcvd + pkts_missed + pkts_sent
                c_timestamp = round_output.loc[round_output.str.contains('C rcvd') | round_output.str.contains('C missed') | round_output.str.contains('C sent')].first_valid_index()

                # Extract D info
                d_ignored_ln = round_output[round_output.str.contains('D ignored')]
                d_accepted   = round_output[round_output.str.contains('D rcvd')].shape[0]
                d_inferior   = d_ignored_ln[d_ignored_ln.str.contains('<')].shape[0]
                d_ignored    = d_ignored_ln.shape[0] - d_inferior

                row = {'timestamp': round_end, 'test_id': test_id, 'node_id': node_id, 'protocol': 'Demos', 'round': curr_round, 'curr_state': curr_state,
                       'demand': curr_demand, 'network_size': curr_net_size, 'curr_leader': curr_leader, 'is_leader': is_leader, 'voted': has_voted,
                       'p_leader_proposed': p_leader_proposed,
                       'v_success': v_counter >= 0, 'v_counter': v_counter, 'v_nr_votes': v_nr_votes, 'v_vote_id': v_vote_id, 'v_vote_phase': v_vote_phase,
                       'v_rx_success': v_rx_success, 'v_rx_done': v_rx_done, 'v_tx_success': v_tx_success,
                       'n_notification_sent': n_notification_sent, 'n_notification_rcvd': n_notification_rcvd,
                       's_leader_id': s_leader_id, 's_sched_hash': s_sched_hash, 's_notification_id': s_notification_id,
                       'pkts_rcvd': pkts_rcvd, 'pkts_missed': pkts_missed, 'pkts_sent': pkts_sent, 'pkts_tot': pkts_total, 'c_timestamp': c_timestamp if c_timestamp is not None else round_end,
                       'd_accepted': d_accepted, 'd_inferior': d_inferior, 'd_ignored': d_ignored}
                rows_list.append(row)

                round_start = round_end

        node_df = pd.DataFrame(rows_list)

        # Find network statistics
        stats_copy = node_df.copy()
        stats_copy.set_index('c_timestamp', drop=False, inplace=True)
        stats_copy.sort_index(inplace=True)

        # Filter IDs
        if filter_ids is not None:
            stats_copy = stats_copy[~stats_copy['node_id'].isin(filter_ids)]
            self._logger.info('Dropped statistics from %u nodes for network statistics' % (len(filter_ids),))

        rows_list    = []
        curr_time    = stats_copy.index.min() - 0.25
        search_scope = 3  # How long to search for prints after the given event [s]

        while curr_time <= stats_copy.index.max():
            curr_stats = stats_copy[curr_time:(curr_time + search_scope)]
            curr_round = curr_stats['round'].median()

            # Votes
            candidates = []
            votes_node = dict.fromkeys([node_id for node_id in set(node_ids + FL_MOBILE_IDS)], 0)
            for stats in curr_stats.itertuples():
                if stats.v_vote_id > 0:
                    candidates.append(stats.v_vote_id)
                    votes_node[stats.v_vote_id] = votes_node[stats.v_vote_id] + 1

            # Leaders and schedules
            leaders     = []
            for stats in curr_stats.itertuples():
                if stats.s_leader_id > 0:
                    leaders.append(stats.s_leader_id)

            # Total number of packets
            curr_pkts_sent   = curr_stats['pkts_sent'].sum()
            curr_pkts_rcvd   = curr_stats['pkts_rcvd'].sum()
            curr_pkts_missed = curr_stats['pkts_missed'].sum()
            c_timestamp      = curr_stats['c_timestamp'].median()

            # Network parameters
            network_size_avg = curr_stats['network_size'].median()
            network_size_min = curr_stats['network_size'].min()
            network_size_max = curr_stats['network_size'].max()

            row = {'timestamp': curr_stats['timestamp'].median(), 'test_id': test_id, 'protocol': 'Demos', 'round': curr_round,
                   'N_avg': network_size_avg, 'N_min': network_size_min, 'N_max': network_size_max,
                   'candidates': len(set(candidates)), 'votes_node': votes_node,
                   'leaders': len(set(leaders)),
                   'n_sent': (curr_stats['n_notification_sent'] > 0).sum(), 'n_rcvd': (curr_stats['n_notification_rcvd'] > 0).sum(),
                   'pkts_sent': curr_pkts_sent, 'pkts_rcvd': curr_pkts_rcvd, 'pkts_missed': curr_pkts_missed, 'c_timestamp': c_timestamp,
                   'd_accepted': curr_stats['d_accepted'].sum(), 'd_inferior': curr_stats['d_inferior'].sum(), 'd_ignored': curr_stats['d_ignored'].sum()}
            rows_list.append(row)

            if fixed_stats_interval:
                curr_time = curr_time + search_scope
            else:
                curr_time = stats_copy[(curr_time + search_scope):].index.min()

        network_df = pd.DataFrame(rows_list)

        if fixed_stats_interval:
            # Fixed intervals distort the measurements, as nodes do not output their data at the same time -> we need to apply a rolling filter
            network_df['pkts_rcvd_raw']                                = network_df['pkts_rcvd']
            network_df['pkts_rcvd']                                    = network_df['pkts_rcvd_raw'].rolling(window=3, center=True, closed='right').mean()
            network_df.at[network_df.first_valid_index(), 'pkts_rcvd'] = network_df.at[network_df.first_valid_index(), 'pkts_rcvd_raw']  # Initial value must be set by raw measurements, rolling window replaces it with NaN

            # Add first point for Demos in PRR
            start_df   = pd.DataFrame({'timestamp': df['timestamp'].min(), 'pkts_rcvd': 0, 'pkts_rcvd_raw': 0, 'test_id': test_id}, index=[0])
            network_df = pd.concat([network_df, start_df])

        return [node_df, network_df]

    def extract_lwb_logs(self, test_id, path=FILE_DIR + FL_TEST_DIR, filter_ids=None, mobile_node=None, apply_rolling_filter=True):

        test_path   = path + '/' + str(test_id)
        serial_path = test_path + FL_SERIAL

        if not isdir(test_path):
            self._logger.warning('Invalid test directory: %s' % (test_path,))
            return False
        elif not isfile(serial_path):
            self._logger.warning('Invalid path to Serial file: %s' % (serial_path,))
            return False

        # Read Serial file
        col_names = ['timestamp', 'observer_id', 'node_id', 'direction', 'output', 'slots']
        serial_df = pd.read_csv(serial_path, names=col_names, skiprows=1, usecols=range(len(col_names)))

        df = serial_df[['timestamp', 'node_id', 'output', 'slots']].copy()
        df.set_index('timestamp', drop=False, inplace=True)
        df.sort_index(inplace=True)

        # Also add info from mobile node if available
        if mobile_node is not None:
            mobile_df = pd.read_csv(test_path + FL_MOBILE_SERIAL, sep=']', names=['timestamp', 'output'])
            mobile_df['timestamp']  = mobile_df['timestamp'].str[len('['):].astype(float)      # Remove initial '[' and cast to int
            mobile_df['timestamp'] += df['timestamp'].min()                                    # Offset start time
            output_col              = mobile_df['output'].str[len(' [90m'):-len('[0m')].str.split(',', expand=True)  # Remove initial and ending characters for GTKTERM
            mobile_df['output']     = output_col[0]                                            # Add first part as separate column
            mobile_df['slots']      = output_col[1]                                            # Add slots as separate column
            mobile_df['node_id']    = mobile_node
            df = pd.concat([df, mobile_df])
            df.set_index('timestamp', drop=False, inplace=True)
            df.sort_index(inplace=True)

        # Find statistics per node
        node_ids  = sorted(df['node_id'].unique())
        rows_list = []

        for node_id in node_ids:
            curr_serial = df.loc[df['node_id'] == node_id]

            # Go through each round and gather statistics
            dc_info     = curr_serial.loc[curr_serial['output'].str.contains('task_post: DC - '), 'output']
            round_start = 0

            for index, output in dc_info.items():
                # Fetch all output from this round (some is potentially printed after the DC print)
                buffer_s   = 0.1
                curr_round = curr_serial[round_start:(index + buffer_s)]

                # Find the sync information
                curr_slots = curr_round.loc[curr_round['output'].str.contains('\| T: '), 'slots']
                pkts_total = int(curr_slots.iloc[0].split()[-1])

                # If host node just reset, no packets have been scheduled but the output still shows all as they were already added -> manually set this input to 0
                if curr_round['output'].str.contains('host node').any():
                    pkts_total = 0

                # Find number of missed packets
                pkts_missed   = curr_round[curr_round['output'].str.contains('Missed pkt')].shape[0]
                pkts_sent     = curr_round[curr_round['output'].str.contains('Pkt sent')  ].shape[0]
                pkts_received = curr_round[curr_round['output'].str.contains('Rcvd   pkt')].shape[0]

                row = {'timestamp': index, 'test_id': test_id, 'node_id': node_id, 'protocol': 'LWB', 'c_timestamp': index,
                       'pkts_rcvd': pkts_received, 'pkts_missed': pkts_missed, 'pkts_sent': pkts_sent, 'pkts_tot': pkts_total}
                rows_list.append(row)

                round_start = index + buffer_s

        node_df = pd.DataFrame(rows_list)

        # Find network statistics
        stats_copy = node_df.copy()
        stats_copy.set_index('timestamp', drop=False, inplace=True)
        stats_copy.sort_index(inplace=True)

        # Filter IDs
        if filter_ids is not None:
            stats_copy = stats_copy[~stats_copy['node_id'].isin(filter_ids)]
            self._logger.info('Dropped statistics from %u nodes for network statistics' % (len(filter_ids),))

        rows_list    = []
        curr_time    = stats_copy.index.min()
        search_scope = 2.5  # How long to search for prints after the given event [s]

        while curr_time <= stats_copy.index.max():
            curr_stats = stats_copy[curr_time:(curr_time + search_scope)]

            # DD success rate
            curr_dd_success = 1 - curr_stats['pkts_missed'].sum() / (curr_stats['pkts_rcvd'].sum() + curr_stats['pkts_missed'].sum())  if (curr_stats['pkts_rcvd'].sum() + curr_stats['pkts_missed'].sum()) > 0 else 0

            # Total number of packets
            curr_pkts_sent   = curr_stats['pkts_sent'].sum()
            curr_pkts_rcvd   = curr_stats['pkts_rcvd'].sum()
            curr_pkts_missed = curr_stats['pkts_missed'].sum()

            row = {'timestamp': curr_stats['timestamp'].min(), 'test_id': test_id, 'protocol': 'LWB', 'c_timestamp': curr_stats['timestamp'].min(),
                   'dd_success': curr_dd_success, 'pkts_sent': curr_pkts_sent, 'pkts_rcvd': curr_pkts_rcvd, 'pkts_missed': curr_pkts_missed}
            rows_list.append(row)

            curr_time = stats_copy[(curr_time + search_scope):].index.min()

        network_df = pd.DataFrame(rows_list)

        if apply_rolling_filter:
            # Fixed intervals distort the measurements, as nodes do not output their data at the same time -> we need to apply a rolling filter
            network_df['pkts_rcvd_raw']                                = network_df['pkts_rcvd']
            network_df['pkts_rcvd']                                    = network_df['pkts_rcvd_raw'].rolling(window=3, center=True, closed='right').mean()
            network_df.at[network_df.first_valid_index(), 'pkts_rcvd'] = network_df.at[network_df.first_valid_index(), 'pkts_rcvd_raw']  # Initial value must be 0, rolling window replaces it with NaN

        return [node_df, network_df]

    @staticmethod
    def compute_election_latency(election_delay, elections_per_round=3, round_period_s=3):
        round_offset = int(election_delay / round_period_s)
        phase_offset = election_delay % round_period_s

        if   phase_offset < 0.265:
            logging.error('Re-elected previous leader')
            return np.NaN
        elif phase_offset < 0.445:
            return (elections_per_round - 1) * round_offset + 1
        elif phase_offset < 0.625:
            return (elections_per_round - 1) * round_offset + 2
        else:
            logging.error('Invalid phase offset: %6.4fs' % (phase_offset,))
            return np.NaN

    def extract_election_stats(self, test_id, path=FILE_DIR + FL_TEST_DIR):

        test_path = path + '/' + str(test_id)
        gpio_path = test_path + FL_GPIO

        if not isdir(test_path):
            self._logger.warning('Invalid test directory: %s' % (test_path,))
            return False
        elif not isfile(gpio_path):
            self._logger.warning('Invalid path to GPIO tracing file: %s' % (gpio_path,))
            return False


        # Read GPIO file
        col_names = ['timestamp', 'observer_id', 'node_id', 'pin_name', 'value']
        gpio_df   = pd.read_csv(gpio_path, names=col_names, skiprows=1, float_precision='round_trip')

        df = gpio_df[['timestamp', 'observer_id', 'node_id', 'pin_name', 'value']].copy()
        df.set_index('timestamp', drop=False, inplace=True)
        df.sort_index(inplace=True)

        # Filter unused pins
        FL_STATE_PINS = ['INT1', 'INT2', 'SIG1']
        used_pins     = FL_STATE_PINS
        df            = df[df['pin_name'].isin(used_pins)]

        # Filter initial values where each node is set at t=0
        df = df[(df.first_valid_index() + 0.000001):]


        # Analyse trace
        node_ids       = sorted(df['node_id'].unique())
        affected_nodes = sorted(df.loc[df['pin_name'] == FL_ACTUATION_PIN, 'node_id'].unique())

        first_election = df[(df['pin_name'] == 'INT1') & (df['value'] == 1)].first_valid_index()
        election_start = df.loc[(df['pin_name'] == FL_ACTUATION_PIN) & (df['value'] == 1), 'timestamp'].min()
        trace_before   = df[(first_election + 0.001):election_start]
        trace_after    = df[election_start:]

        # Filter traces after election started by nodes that are still active
        trace_after = trace_after[~trace_after['node_id'].isin(affected_nodes)]

        # Get elections
        elections          = trace_after[(trace_after['pin_name'] == 'INT1') & (trace_after['value'] == 1)]
        elected_nodes      = elections['node_id']
        election_delays    = (elections['timestamp'] - election_start)
        election_latencies = election_delays.apply(self.compute_election_latency)

        if election_latencies.isna().any():
            self._logger.warning('Invalid election latencies in test %u', test_id)

        # Get disturbance before start of the election
        instabilities = trace_before.loc[(trace_before['pin_name'] == 'INT1') & (trace_before['value'] == 1), 'node_id'].tolist()

        # Get yielding nodes
        yields = df.loc[(df['pin_name'] == 'INT2') & (df['value'] == '1'), 'node_id'].tolist()

        return [election_latencies, election_delays, elected_nodes, instabilities, yields]


# ----------------------------------------------------------------------------------------------------------------------
# Main
# ----------------------------------------------------------------------------------------------------------------------


if __name__ == "__main__":

    # Initialization
    try:
        # Create DemosAnalyser to store parameters
        analyser = DemosAnalyser()

        # Parse arguments
        parser = ArgumentParser(description='usage: %(prog)s [options]')

        parser.add_argument('-d', '--download-tests', dest='download', action='store_true',
                            help='Download locally scheduled tests.')
        parser.add_argument('-p', '--path', type=str, dest='path',
                            help='Path to respective file.', metavar='STRING')
        parser.add_argument('-t', '--test', dest='test', action='store_true',
                            help='Schedule new test.')
        parser.add_argument('-m', '--message', type=str, dest='msg',
                            help='Message which will be added to the XML and the log.', metavar='STRING')
        parser.add_argument('-a', '--add-actuations', dest='add_actuations', action='store_true',
                            help='Add GPIO actuations stored in \'%s\'.' % (FL_ACTUATION,))
        parser.add_argument('-r', '--add-resets', dest='add_resets', action='store_true',
                            help='Add GPIO resets stored in \'%s\'.' % (FL_ACTUATION,))
        parser.add_argument('-l', '--length', type=int, dest='length_s',
                            help='Test duration.', metavar='INT', default=300)
        parser.add_argument('-n', '--network-size', type=int, dest='network_size',
                            help='Network size.', metavar='INT', default=len(FL_NETWORK_LARGE))
        parser.add_argument('-e', '--measure-energy', dest='enable_power_profiling', action='store_true',
                            help='Enable power profiling.')
        parser.add_argument('-s', '--store-metrics', dest='store_metrics', action='store_true',
                            help='Store metrics for analysis.')
        parser.add_argument('-g', '--generate-actuations', dest='generate_actuations', action='store_true',
                            help='Generate new random actuations using \'%s\'.' % (FL_GENERATION,))
        parser.add_argument('-b', '--binary-patch', type=str, dest='patch_dict',
                            help='Apply given dictionary for binary patching.', metavar='DICT', default=None)
        cli_args = parser.parse_args()

        # Check arguments
        if cli_args.path is not None and not isinstance(cli_args.path, str):
            raise TypeError('Invalid path to .log files: %s' % (cli_args.path,))
    except Exception as ex:
        logging.error('Experienced an error: %s' % (ex,))
        logging.shutdown()

        # Exit program
        sys.exit(1)

    # Setup handlers for root logger
    root = logging.getLogger()
    root.setLevel(DEFAULT_LOG_LEVEL)
    root_formatter = logging.Formatter(fmt=LOG_FORMAT, datefmt=LOG_DATE_FORMAT)

    # Clear old handlers and setup new ones
    if root.hasHandlers():
        root.handlers.clear()

    if analyser.LOG_LVL is not None:
        # Pipe root logger to stdout (StreamHandler)
        handler = logging.StreamHandler()
        handler.setLevel(analyser.LOG_LVL)
        handler.setFormatter(root_formatter)
        root.addHandler(handler)

    # Print debug options
    logging.debug('Demos Analysis CLI options:')
    for arg in vars(cli_args):
        logging.debug(' {}:\t {}'.format(arg, getattr(cli_args, arg) or ''))

    logging.debug('Demos Analysis script is running...')

    try:
        # Check arguments
        if   cli_args.download:
            # Fetch all scheduled tests
            analyser.download_tests(cli_args.path)
        elif cli_args.store_metrics:
            # Extract the statistics of all scheduled tests and store them as a separate file
            analyser.store_metrics(cli_args.path)
        elif cli_args.test:
            # Create text XML
            logging.debug("Generating FlockLab XML...")
            if cli_args.generate_actuations:
                FL_ACTUATION = FL_TEST_DIR + FL_ACTUATION[:-len('.log')] + '_' + str(randint(1, 999)) + '.log'
                os.system('%s > %s' % (FILE_DIR + FL_GENERATION, FILE_DIR + FL_ACTUATION,))
                logging.info("Generated random actuations in \'%s\'" % (FL_ACTUATION,))

            if cli_args.add_actuations:
                analyser.generate_xml(path=cli_args.path, comment_str=cli_args.msg, test_time_s=cli_args.length_s, nr_nodes=cli_args.network_size, enable_power_profiling=cli_args.enable_power_profiling, apply_patch=cli_args.patch_dict, gpio_actuations=analyser.load_gpio_actuations())
            elif cli_args.add_resets:
                analyser.generate_xml(path=cli_args.path, comment_str=cli_args.msg, test_time_s=cli_args.length_s, nr_nodes=cli_args.network_size, enable_power_profiling=cli_args.enable_power_profiling, apply_patch=cli_args.patch_dict, gpio_actuations=analyser.load_gpio_actuations(pin=FL_RESET_PIN))
            else:
                analyser.generate_xml(path=cli_args.path, comment_str=cli_args.msg, test_time_s=cli_args.length_s, nr_nodes=cli_args.network_size, enable_power_profiling=cli_args.enable_power_profiling, apply_patch=cli_args.patch_dict)
            logging.debug("Created FlockLab XML")

            # Send test to FL
            logging.debug("Sending XML to server...")
            analyser.upload_xml(meta_data=cli_args.msg)
            logging.debug("Sent XML to server")
    except Exception as ex:
        logging.error('Experienced an error: %s' % (ex,))
        logging.shutdown()
