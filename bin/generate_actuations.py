#!/usr/bin/env python3

from random import random


FL_NODE_IDS     = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 16, 19, 20, 21, 22, 23, 24, 26, 27, 28, 29, 30, 31, 32]
FL_EXCLUDED_IDS = [4, 5, 10]

min_offset_s   = 0
max_offset_s   = 0
min_duration_s = 0
max_duration_s = 60

# Update node IDs
node_ids = set(FL_NODE_IDS) - set(FL_EXCLUDED_IDS)

# Generate actuations
for node in node_ids:
    print("%2i, %11.7f, %11.7f" % (node, min_offset_s + random() * max_offset_s, min_duration_s + random() * max_duration_s,))
